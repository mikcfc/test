package com.weather.miqo.weatherforecastdemo.model;

public class CountryItem {

    public String mCountry;
    public String mTemperature;
    public String mPressure;
    public String mHumidity;
    public String mPrecipitation;

    public CountryItem(String country, String temperature, String pressure, String humidity, String precipitation) {
        mCountry = country;
        mTemperature = temperature;
        mPressure = pressure;
        mHumidity = humidity;
        mPrecipitation = precipitation;
    }
}
