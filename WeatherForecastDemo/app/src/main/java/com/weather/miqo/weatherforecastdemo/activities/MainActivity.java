package com.weather.miqo.weatherforecastdemo.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.weather.miqo.weatherforecastdemo.R;
import com.weather.miqo.weatherforecastdemo.adapters.CountriesAdapter;
import com.weather.miqo.weatherforecastdemo.network.NetworkUtil;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private TextView mNoConnection;

    private BroadcastReceiver mNetworkStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() != null && intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                if (NetworkUtil.isNetworkAvailable()) {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mNoConnection.setVisibility(View.GONE);
                } else {
                    mRecyclerView.setVisibility(View.GONE);
                    mNoConnection.setVisibility(View.VISIBLE);
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView = findViewById(R.id.countries_list);
        mNoConnection = findViewById(R.id.no_connection);
        CountriesAdapter countriesAdapter = new CountriesAdapter();
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(countriesAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            if (mNetworkStateReceiver != null) {
                unregisterReceiver(mNetworkStateReceiver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        registerReceiver(mNetworkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            if (mNetworkStateReceiver != null) {
                unregisterReceiver(mNetworkStateReceiver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
