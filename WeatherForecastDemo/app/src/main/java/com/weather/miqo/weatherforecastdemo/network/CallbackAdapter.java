package com.weather.miqo.weatherforecastdemo.network;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class CallbackAdapter<T> implements Callback<T> {

    @Override
    public void onResponse(Call<T> call, Response<T> response) {

    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {

    }

    public void success(T o) {

    }

    public void failure(Throwable error) {

    }
}
