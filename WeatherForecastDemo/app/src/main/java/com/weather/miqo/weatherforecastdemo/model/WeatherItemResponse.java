package com.weather.miqo.weatherforecastdemo.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeatherItemResponse {

    @SerializedName("main")
    public MainBean main;

    @SerializedName("weather")
    public List<WeatherBean> weather;

    public static class MainBean {

        @SerializedName("temp")
        public double temp;

        @SerializedName("pressure")
        public double pressure;

        @SerializedName("humidity")
        public int humidity;
    }

    public static class WeatherBean {

        @SerializedName("main")
        public String main;
    }
}
