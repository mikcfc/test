package com.weather.miqo.weatherforecastdemo.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtil {

    private static Context mContext;

    public static void init(Context context) {
        mContext = context;
    }

    public static boolean isNetworkAvailable() {
        NetworkInfo networkInfo = getNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    private static NetworkInfo getNetworkInfo() {
        return ((ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
    }
}
