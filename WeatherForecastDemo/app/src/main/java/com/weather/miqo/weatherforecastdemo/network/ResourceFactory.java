package com.weather.miqo.weatherforecastdemo.network;

import android.annotation.SuppressLint;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class ResourceFactory {

    static final String OPEN_WEATHER_API_KEY = "c75a0fcad67b7158133776b3ef38166c";

    private static Retrofit mRootResourceFactory;

    @SuppressLint("TrustAllX509TrustManager")
    static <T> T createResource(Class<T> resourceClass) {
        if (mRootResourceFactory == null) {
            try {
                final TrustManager[] trustAllCerts = new TrustManager[]{
                        new X509TrustManager() {
                            @Override
                            public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                            }


                            @Override
                            public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                            }

                            @Override
                            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                return new java.security.cert.X509Certificate[]{};
                            }
                        }
                };

                final SSLContext sslContext = SSLContext.getInstance("SSL");
                String DEFAULT_DATE_FORMAT = "dd'-'MM'-'yyyy'T'HH':'mm':'ss";
                String MAIN_URL = "https://api.openweathermap.org/data/2.5/";

                try {
                    sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                }

                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                OkHttpClient okHttpClient = new OkHttpClient.Builder()
                        .connectTimeout(5, TimeUnit.MINUTES)
                        .readTimeout(5, TimeUnit.MINUTES)
                        .addInterceptor(interceptor)
                        .build();
                Gson gson = new GsonBuilder().setLenient().setDateFormat(DEFAULT_DATE_FORMAT).create();
                mRootResourceFactory = new Retrofit.Builder()
                        .baseUrl(MAIN_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .client(okHttpClient)
                        .build();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }

        return mRootResourceFactory.create(resourceClass);
    }
}
