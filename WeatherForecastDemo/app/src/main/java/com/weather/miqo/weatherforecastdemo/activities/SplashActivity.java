package com.weather.miqo.weatherforecastdemo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.weather.miqo.weatherforecastdemo.R;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {

    private static final int SPLASH_ACTIVITY_DELAY = 2000;
    private static final int ANIMATION_DURATION = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Animation animation = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
        final Intent intent = new Intent(this, MainActivity.class);
        ImageView splashLogo = findViewById(R.id.splash_logo);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        animation.setDuration(ANIMATION_DURATION);
        splashLogo.startAnimation(animation);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                startActivity(intent);
                finish();
            }
        }, SPLASH_ACTIVITY_DELAY);
    }
}
