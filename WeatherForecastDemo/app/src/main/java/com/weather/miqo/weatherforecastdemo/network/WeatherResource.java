package com.weather.miqo.weatherforecastdemo.network;

import com.weather.miqo.weatherforecastdemo.model.WeatherItemResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherResource {

    @GET("weather")
    Call<WeatherItemResponse> getWeatherForCountry(@Query("appid") String appId,
                                                   @Query("q") String country);
}
