package com.weather.miqo.weatherforecastdemo;

import android.app.Application;

import com.weather.miqo.weatherforecastdemo.network.NetworkUtil;
import com.weather.miqo.weatherforecastdemo.network.WeatherManager;

public class WeatherForecastDemoApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        NetworkUtil.init(this);
        WeatherManager.initWeatherManager();
    }
}
