package com.weather.miqo.weatherforecastdemo.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.weather.miqo.weatherforecastdemo.R;
import com.weather.miqo.weatherforecastdemo.model.CountryItem;
import com.weather.miqo.weatherforecastdemo.model.WeatherItemResponse;
import com.weather.miqo.weatherforecastdemo.network.CallbackAdapter;
import com.weather.miqo.weatherforecastdemo.network.WeatherManager;

import java.util.ArrayList;

public class CountriesAdapter extends RecyclerView.Adapter<CountriesAdapter.ViewHolder> {

    private ArrayList<CountryItem> mCountriesList;

    public CountriesAdapter() {
        mCountriesList = new ArrayList<>();
        mCountriesList.add(new CountryItem("Armenia", "-", "-", "-", "-"));
        mCountriesList.add(new CountryItem("USA", "-", "-", "-", "-"));
        mCountriesList.add(new CountryItem("Russia", "-", "-", "-", "-"));
        mCountriesList.add(new CountryItem("Cuba", "-", "-", "-", "-"));
        mCountriesList.add(new CountryItem("Brazil", "-", "-", "-", "-"));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_country, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mCountry.setText(mCountriesList.get(position).mCountry);
        holder.mTemperature.setText(mCountriesList.get(position).mTemperature);
        holder.mPressure.setText(mCountriesList.get(position).mPressure);
        holder.mHumidity.setText(mCountriesList.get(position).mHumidity);
        holder.mPrecipitation.setText(mCountriesList.get(position).mPrecipitation);
    }

    private CountryItem getItem(int position) {
        return mCountriesList.get(position);
    }

    @Override
    public int getItemCount() {
        return mCountriesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mCountry;
        private TextView mTemperature;
        private TextView mPressure;
        private TextView mHumidity;
        private TextView mPrecipitation;

        ViewHolder(View itemView) {
            super(itemView);
            mCountry = itemView.findViewById(R.id.country_placeholder);
            mTemperature = itemView.findViewById(R.id.temperature_placeholder);
            mPressure = itemView.findViewById(R.id.pressure_placeholder);
            mHumidity = itemView.findViewById(R.id.humidity_placeholder);
            mPrecipitation = itemView.findViewById(R.id.precipitation_placeholder);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            getWeatherForCountry(getItem(getAdapterPosition()).mCountry, getAdapterPosition());
        }

        private void showErrorMessage() {
            Toast.makeText(itemView.getContext(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
        }

        private void getWeatherForCountry(String country, final int position) {
            WeatherManager.getInstance().getWeather(country, new CallbackAdapter<WeatherItemResponse>() {
                @Override
                public void success(WeatherItemResponse weatherItemResponse) {
                    if (weatherItemResponse != null) {
                        getItem(position).mTemperature = String.valueOf((int) weatherItemResponse.main.temp - 273) + " °C";
                        getItem(position).mPressure = String.valueOf(weatherItemResponse.main.pressure) +" hPa";
                        getItem(position).mHumidity = String.valueOf(weatherItemResponse.main.humidity + " RH");
                        getItem(position).mPrecipitation = String.valueOf(weatherItemResponse.weather.get(0).main);
                        notifyItemChanged(position);
                    } else {
                        showErrorMessage();
                    }
                }

                @Override
                public void failure(Throwable body) {
                    showErrorMessage();
                }
            });
        }
    }
}
