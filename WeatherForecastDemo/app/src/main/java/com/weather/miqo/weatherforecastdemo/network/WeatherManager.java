package com.weather.miqo.weatherforecastdemo.network;

import com.weather.miqo.weatherforecastdemo.model.WeatherItemResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherManager {

    private static WeatherManager sWeatherManager;
    private WeatherResource mWeatherResource;

    public static void initWeatherManager() {
        if (sWeatherManager == null) {
            sWeatherManager = new WeatherManager();
        }
    }

    public static WeatherManager getInstance() {
        return sWeatherManager;
    }

    private WeatherManager() {
        mWeatherResource = ResourceFactory.createResource(WeatherResource.class);
    }

    public void getWeather(String country, final CallbackAdapter<WeatherItemResponse> callback) {
        mWeatherResource.getWeatherForCountry(ResourceFactory.OPEN_WEATHER_API_KEY, country).enqueue(new Callback<WeatherItemResponse>() {
            @Override
            public void onResponse(Call<WeatherItemResponse> stringCall, Response<WeatherItemResponse> response) {
                if (200 == response.code()) {
                    callback.success(response.body());
                } else {
                    callback.failure(new Throwable());
                }
            }

            @Override
            public void onFailure(Call<WeatherItemResponse> call, Throwable t) {
                callback.failure(t);
            }
        });
    }
}
